"""
This module is part of the 'py-gesetze' package,
which is released under GPL-3.0-only license.
"""

from .norm import Norm

__all__ = ["Norm"]
