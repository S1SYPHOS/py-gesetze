"""
This module is part of the 'py-gesetze' package,
which is released under GPL-3.0-only license.
"""

from click.testing import CliRunner

from gesetze.cli import cli


def test_cli():
    """
    Tests CLI interface
    """

    # Setup
    # (1) CLI runner
    runner = CliRunner()

    # Run function #1
    result1 = runner.invoke(cli)

    # Assert result
    assert result1.exit_code == 0

    # Run function #2
    result2 = runner.invoke(cli, ["-v", "scrape", "-p", "invalid"])

    # Assert result
    assert result2.exit_code == 1
