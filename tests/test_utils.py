"""
This module is part of the 'py-gesetze' package,
which is released under GPL-3.0-only license.
"""

from pathlib import Path

from pytest import raises

from gesetze import analyze, extract, roman2arabic

# Setup
# (1) Directory containing fixtures
fixtures = Path(__file__).parent / "fixtures"

# (2) Text (with legal references)
with open(fixtures / "text.html", encoding="utf-8") as file:
    text = file.read()

# (3) Text (without legal references)
with open(fixtures / "empty.html", encoding="utf-8") as file:
    empty = file.read()


def test_analyze() -> None:
    """
    Tests 'analyze()' helper
    """

    # Setup
    # (1) Norms
    norms = {
        # Section sign
        "sign": {
            "§ 1 BGB": {
                "norm": "1",
                "gesetz": "BGB",
            },
            "§§ 1 BGB": {
                "norm": "1",
                "gesetz": "BGB",
            },
            "<span>&sect; 1 BGB</span>": {
                "norm": "1",
                "gesetz": "BGB",
            },
            "Artikel 1 BGB": {
                "norm": "1",
                "gesetz": "BGB",
            },
            "Art. 1 BGB": {
                "norm": "1",
                "gesetz": "BGB",
            },
        },
        # Section
        "section": {
            "§ 1 BGB": {
                "norm": "1",
                "gesetz": "BGB",
            },
            "§ 1a BGB": {
                "norm": "1a",
                "gesetz": "BGB",
            },
        },
        # Subsection
        "subsection": {
            "§ 1 Absatz 2 BGB": {
                "norm": "1",
                "absatz": "2",
                "gesetz": "BGB",
            },
            "§ 1 Abs. 2 BGB": {
                "norm": "1",
                "absatz": "2",
                "gesetz": "BGB",
            },
            "§ 1 II BGB": {
                "norm": "1",
                "absatz": "II",
                "gesetz": "BGB",
            },
            "§ 1 Absatz 2a BGB": {
                "norm": "1",
                "absatz": "2a",
                "gesetz": "BGB",
            },
            "§ 1 Abs. 2a BGB": {
                "norm": "1",
                "absatz": "2a",
                "gesetz": "BGB",
            },
            "§ 1 IIa BGB": {
                "norm": "1",
                "absatz": "IIa",
                "gesetz": "BGB",
            },
        },
        # Sentence
        "sentence": {
            "§ 1 Satz 1 BGB": {
                "norm": "1",
                "satz": "1",
                "gesetz": "BGB",
            },
            "§ 1 S. 1 BGB": {
                "norm": "1",
                "satz": "1",
                "gesetz": "BGB",
            },
        },
        # Number
        "number": {
            "§ 1 Nummer 1 BGB": {
                "norm": "1",
                "nr": "1",
                "gesetz": "BGB",
            },
            "§ 1 Nr. 1 BGB": {
                "norm": "1",
                "nr": "1",
                "gesetz": "BGB",
            },
        },
        # Letter
        "letter": {
            "§ 1 litera a BGB": {
                "norm": "1",
                "lit": "a",
                "gesetz": "BGB",
            },
            "§ 1 lit. a BGB": {
                "norm": "1",
                "lit": "a",
                "gesetz": "BGB",
            },
            "§ 1 Buchstabe a BGB": {
                "norm": "1",
                "lit": "a",
                "gesetz": "BGB",
            },
            "§ 1 Buchst. a BGB": {
                "norm": "1",
                "lit": "a",
                "gesetz": "BGB",
            },
        },
        # Law
        "law": {
            "§ 1 BGB": {
                "norm": "1",
                "gesetz": "BGB",
            },
            "§ 1 SGB V": {
                "norm": "1",
                "gesetz": "SGB V",
            },
        },
    }

    # Run function
    for values in norms.values():
        for string, expected in values.items():
            # Assert result
            assert analyze(string) == expected


def test_analyze_empty() -> None:
    """
    Tests 'analyze()' (no legal references)
    """

    # Setup
    # (1) Norms
    norms = [
        "",
        "§ 1 by itself == useless",
        "This is for educational purposes only",
    ]

    # Run function
    for string in norms:
        # Assert result
        assert analyze(string) == None


def test_extract() -> None:
    """
    Tests 'extract()'
    """

    # Assert result
    assert extract(text) == [
        "Art. 12 Abs. 1 GG",
        "&sect; 1 ZPO",
        "§ 433 II BGB",
        "§ 1a BGB",
        "§ 1 GGGG",
        "Art. 2 Abs. 2 DSGVO",
    ]


def test_extract_empty() -> None:
    """
    Tests 'extract()' (text without legal references)
    """

    # Assert result
    assert extract(empty) == []


def test_roman2arabic() -> None:
    """
    Tests 'roman2arabic()'
    """

    # Setup
    # (1) Roman numerals
    romans = {
        "II": 2,
        "IV": 4,
        "VI": 6,
        "IX": 9,
        "XIV": 14,
        "LXIX": 69,
        "CXLVII": 147,
    }

    # Run function
    for roman, expected in romans.items():
        # Assert ..
        # (1) result for uppercase
        assert roman2arabic(roman) == expected

        # (2) result for lowercase
        assert roman2arabic(roman.lower()) == expected


def test_roman2arabic_invalid() -> None:
    """
    Tests 'roman2arabic()' (invalid input)
    """

    # Setup
    # (1) Roman numerals
    invalid = [
        "",
        "Y",
        "AZ",
        "OMG",
        "LOL",
        "ROFL",
    ]

    for roman in invalid:
        # Assert exception
        with raises(Exception):
            # Run function
            roman2arabic(roman)
