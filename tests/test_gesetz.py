"""
This module is part of the 'py-gesetze' package,
which is released under GPL-3.0-only license.
"""

from pathlib import Path

from bs4 import BeautifulSoup as bs

from gesetze import Gesetz

# Setup
# (1) Directory containing fixtures
fixtures = Path(__file__).parent / "fixtures"

# (2) Text (with legal references)
with open(fixtures / "text.html", encoding="utf-8") as file:
    text = file.read()

# (3) Text (without legal references)
with open(fixtures / "empty.html", encoding="utf-8") as file:
    empty = file.read()


def test_init() -> None:
    """
    Tests providers after setup
    """

    # Run function
    result = Gesetz()

    # Assert result
    assert isinstance(result, Gesetz)
    assert result.providers == ["gesetze", "dejure", "buzer", "lexparency"]


def test_driver_order() -> None:
    """
    Tests order of providers after setup
    """

    # Run function #1
    result1 = Gesetz("lexparency")

    # Assert result
    assert result1.providers == ["lexparency"]

    # Run function #2
    result2 = Gesetz(["gesetze", "buzer"])

    # Assert result
    assert result2.providers == ["gesetze", "buzer"]


def test_validate() -> None:
    """
    Tests 'validate()'
    """

    # Setup
    # (1) Object
    obj = Gesetz()

    # (2) Legal norms
    norms = {
        "§ 433 BGB": True,
        "§ 1a BGB": False,
    }

    # Run function
    for norm, expected in norms.items():
        # Assert result
        assert obj.validate(norm) == expected


def test_validate_empty() -> None:
    """
    Tests 'validate()' (no legal references)
    """

    # Setup
    # (1) Object
    obj = Gesetz()

    # (2) Norms
    norms = [
        "",
        "§ 1 by itself == useless",
        "This is for educational purposes only",
    ]

    # Run function
    for norm in norms:
        # Assert result
        assert obj.validate(norm) == False


def test_gesetzify() -> None:
    """
    Tests 'gesetzify()'
    """

    # Setup
    # (1) Object
    obj = Gesetz()

    # Run function #1
    soup1 = bs(text, "lxml")
    result1 = soup1.find_all("a")

    # Assert result
    assert len(result1) == 0

    # Run function #2
    soup2 = bs(obj.gesetzify(text), "lxml")
    result2 = soup2.find_all("a")

    # Assert result
    assert len(result2) == 4


def test_gesetzify_empty() -> None:
    """
    Tests 'gesetzify()' (text without legal references)
    """

    # Assert result
    assert Gesetz().gesetzify(empty) == empty


def test_gesetzify_callback() -> None:
    """
    Tests 'gesetzify()' with callback
    """

    # Setup
    # (1) Object
    obj = Gesetz()

    # Run function #1
    soup1 = bs(text, "lxml")
    result1 = soup1.find_all("strong")

    # Assert result
    assert len(result1) == 1

    # Run function #2
    def callback(match):
        return f"<strong>{match.group(0)}</strong>"

    soup2 = bs(obj.gesetzify(text, callback), "lxml")
    result2 = soup2.find_all("strong")

    # Assert result
    assert len(result2) == 7


def test_gesetzify_markdownify() -> None:
    """
    Tests 'gesetzify()' & callback 'markdownify'
    """

    # Setup
    # (1) Object
    obj = Gesetz()

    assert (
        obj.gesetzify("Art. 1 GG garantiert die Menschenwürde.", obj.markdownify)
        == "[Art. 1 GG](https://www.gesetze-im-internet.de/gg/art_1.html) "
        + "garantiert die Menschenwürde."
    )


def test_gesetzify_title() -> None:
    """
    Tests 'gesetzify()' & option 'title'
    """

    # Setup
    # (1) Object
    obj = Gesetz()

    # (2) `Title` attributes
    titles = {
        "Art. 12 Abs. 1 GG": {
            "light": "GG",
            "normal": "Grundgesetz für die Bundesrepublik Deutschland",
            "full": "Art 12",
        },
        "§ 1 ZPO": {
            "light": "ZPO",
            "normal": "Zivilprozessordnung",
            "full": "§ 1 Sachliche Zuständigkeit",
        },
        "§ 433 II BGB": {
            "light": "BGB",
            "normal": "Bürgerliches Gesetzbuch",
            "full": "§ 433 Vertragstypische Pflichten beim Kaufvertrag",
        },
        "Art. 2 Abs. 2 DSGVO": {
            "light": "DSGVO",
            "normal": "Verordnung (EU) 2016/679 des Europäischen Parlaments "
            + "und des Rates vom 27. April 2016 zum Schutz natürlicher Personen "
            + "bei der Verarbeitung personenbezogener Daten, zum freien Datenverkehr "
            + "und zur Aufhebung der Richtlinie 95/46/EG",
            "full": "Art.  2 Sachlicher Anwendungsbereich",
        },
    }

    # Run function #1
    soup1 = bs(obj.gesetzify(text), "lxml")
    links1 = soup1.find_all("a")

    for link in links1:
        # Assert result
        assert "title" not in link

    # Change condition `title`
    obj.title = "light"

    # Run function #2
    soup2 = bs(obj.gesetzify(text), "lxml")
    links2 = soup2.find_all("a")

    for link in links2:
        # Assert result
        assert link["title"] == titles[link.text]["light"]

    # Change condition `title`
    obj.title = "normal"

    # Run function #3
    soup3 = bs(obj.gesetzify(text), "lxml")
    links3 = soup3.find_all("a")

    for link in links3:
        # Assert result
        assert link["title"] == titles[link.text]["normal"]

    # Change condition `title`
    obj.title = "full"

    # Run function #4
    soup4 = bs(obj.gesetzify(text), "lxml")
    links4 = soup4.find_all("a")

    for link in links4:
        # Assert result
        assert link["title"] == titles[link.text]["full"]


def test_gesetzify_attributes() -> None:
    """
    Tests 'gesetzify()' & option 'attributes'
    """

    # Setup
    # (1) Object
    obj = Gesetz()

    # (2) Attributes
    attributes = {
        "attr1": "some-value",
        "attr2": "other-value",
    }

    # Run function #1
    soup1 = bs(obj.gesetzify(text), "lxml")
    links1 = soup1.find_all("a")

    for link in links1:
        # Assert result
        for attribute in attributes:
            assert attribute not in link

        assert link["target"] == "_blank"

    # Change condition `attributes`
    obj.attributes = attributes

    # Run function #2
    soup2 = bs(obj.gesetzify(text), "lxml")
    links2 = soup2.find_all("a")

    for link in links2:
        # Assert result
        for attribute, value in attributes.items():
            assert link[attribute] == value
